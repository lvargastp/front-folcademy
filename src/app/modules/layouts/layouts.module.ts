import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { LayoutComponent } from './components/layout/layout.component';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';



@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    LayoutComponent,
    AuthLayoutComponent
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    LayoutComponent,
    AuthLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class LayoutsModule { }

/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { RoutesModule } from './routes.module';
//Container
import * as routesContainer from './container/routes/routes.component';
//Components
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { ViewUserComponent } from './components/view-user/view-user.component';

export const ROUTES: Routes = [
    {
        path: '',
        canActivate: [],
        component: routesContainer.RoutesComponent,
        children: [
            {
                path: '',
                component: HomeComponent,
            },
            {
                path: 'contact',
                canActivate: [],
                component: ContactComponent,
            },
            {
                path: 'all',
                canActivate: [],
                component: AllUsersComponent,
            },
            {
                path: 'user',
                canActivate: [],
                component: ViewUserComponent,
            }
        ]
    }
]

@NgModule({
    imports: [RoutesModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class RoutesRoutingModule {}